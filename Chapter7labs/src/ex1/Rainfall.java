package ex1;

public class Rainfall {
	
	private double[] rainfall;
	
	//filling array with input
	public Rainfall(double[] rain) {
		rainfall = new double[rain.length];
		for(int i = 0; i < rain.length; i++) {

			rainfall[i] = rain[i];
		}
	}
	
	public double calcTotalRain() {
		double total = 0;
		
		for(int i = 0; i < rainfall.length; i++) {
			total += rainfall[i];
		}
		return total;
	}
	
	public double calcAverageRain() {
		double total = 0;
		
		for(int i = 0; i < rainfall.length; i++) {
			total += rainfall[i];
		}
		
		return total / rainfall.length;
	}
	
	public int findMax() {
		int month = 0;
		double max = 0.0;
		
		for(int i = 0; i < rainfall.length; i++) {
			if(rainfall[i] > max) {
				max = rainfall[i];
				month = i + 1;
			}else {
				continue;
			}
		}
		return month;
	}
	
	public int findMin() {
		int month = 0;
		double min = 10;
		
		for(int i = 0; i < rainfall.length; i++) {
			if(rainfall[i] < min) {
				min = rainfall[i];
				month = i + 1;
			}else {
				continue;
			}
		}
		
		return month;
	}

}
