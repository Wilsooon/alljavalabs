package ex1;

import java.util.Scanner;

public class RainfallDriver {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		
		double[] rain = new double[12];
		
		for(int i = 0; i < rain.length; i++) {
			do {
				System.out.print("Enter rainfall for month "+(i+1)+" :");
				rain[i] = sc.nextDouble();
			}
			while(rain[i] < 0);
		}
		
		Rainfall year1 = new Rainfall(rain);
		
		System.out.println("Total rain is " + year1.calcTotalRain() + "\n"
				+ "Average rainfall is " + year1.calcAverageRain() + "\n"
				+ "highest rainfall month is month : " + year1.findMax() + "\n"
				+ "lowest rainfall month is month : " + year1.findMin());

		sc.close();
	}
}
