package ex3;

public class accountValidation {

	private static int[] validAccounts = {56588445, 4520125, 789122, 877541, 1302850, 8080152, 4562555, 5552012, 5050552, 782877, 1250255, 1005231, 654231, 3852085, 7576651, 7881200, 4581002};
	
	
	public static boolean validateAccount(int input) {
		for(int i = 0; i < validAccounts.length; i++) {
			if(validAccounts[i] == input) {
				return true;
			}
		}
		return false;
	}
	
	
}
