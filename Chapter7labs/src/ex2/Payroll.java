package ex2;

public class Payroll {
	
	private int[] employeeId = { 5658845, 4520125, 7895122, 8777541, 8451277, 1302850, 7580489 };
	private int[] hours = new int[7];
	private double[] payrate = new double[7];
	private double[] wages;
	
	
	public void calculateWages()
	{
		wages = new double[7];
		
		for (int index = 0; index < 7; index++)
		{
			wages[index] = hours[index] * payrate[index];
		}
	}
	
	//return current employee id
	public int getId(int id) {
		return employeeId[id];
	}
	
	//set hours for current employee
	public void setHours(int index, int hours) {
		this.hours[index] = hours;
	}
	//set pay rate for current employee
	public void setPayRate(int index, double payrate) {
		this.payrate[index] = payrate;
	}

	
	
	//getters for returning and displaying info
	
	public int[] getEmployeeId(int index) {
		return employeeId;
	}

	public int getHours(int index) {
		
		return hours[index];
	}

	public double getPayrate(int index) {
		return payrate[index];
	}

	public double getWages(int index) {
		return wages[index];
	}
	
	
}
