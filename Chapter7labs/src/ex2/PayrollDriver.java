package ex2;

import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		Payroll payroll = new Payroll();
		
		for(int i = 0; i < 7; ) {
			System.out.println("Please enter hours worked for employe #" + payroll.getId(i));
			int hours = sc.nextInt();
			System.out.println("Please enter payrate for employe #" + payroll.getId(i));
			double rate = sc.nextDouble();
			
			if(hours >= 0 && rate >= 6) {
				payroll.setHours(i, hours);
				payroll.setPayRate(i, rate);
				i++;
			}else {
				System.out.println("Please enter valid payroll info.");
				continue;
			}//end else
			
		}//end for loop
		
		//calculate wages after all info is inputed
		payroll.calculateWages();
		
		for(int i = 0; i < 7; i++) {
			System.out.println("Payroll info for employee #" + payroll.getId(i) + ":");
			System.out.println("Hours workd : " + payroll.getHours(i));
			System.out.println("PayRate : " + payroll.getPayrate(i));
			System.out.println("Total wages : " + payroll.getWages(i));
			System.out.println("-------------------------------------");
		}
		sc.close();
	}//end main
	

}
