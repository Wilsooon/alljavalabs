package ex5;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;
import java.io.*; 

public class Lab5ChangeAccountModification
{
    public static void main(String[] args) throws IOException
    {
        int index = 0;
        int key = 0;


        // initiate scanner for user input
        Scanner input = new Scanner(System.in);

        // ask for charge account
        System.out.println("Looking for a specific Charge Account?");
        System.out.print("Enter a 7 digit integer number\n" +
                "to see if that account is in the computer:  ");
        key = input.nextInt();

        File fl = new File("ChargeAccount.txt");
        Scanner output = new Scanner(fl);

        int[] array = new int[19];


        for (int x = 0; output.hasNext(); ++x)
        {

            array[x] = output.nextInt();

        }

        status(key, array);


        if (status(key, array))
        {
            System.out.println("Account Found");
        }
        else
        {
            System.out.println("Account NOT found");
        }
        output.close();
    }

    public static boolean status(int me, int[] array)
    {
        boolean bool = true;
        int x = Arrays.binarySearch(array, me);
        if (x >= 0)
        {
            return bool = true;
        }
        else
        {
            return bool = false;
        }
    }
}