package ex6;

public class driverLicense {
	
	private static char[] correctAnswers = {'B','D','A','A','C','A','B','A','C','D','B','C','D','A','D','C','C','B','D','A'};
	private static char[] studentAnswers = new char[20];
	
	
	//fill array with student answers
	public static void setStudentAnswers(char[] inputArray) {
		for(int i = 0; i < inputArray.length; i++) {
			studentAnswers[i] = inputArray[i];
		}
	}
	
	//determine how many correct answers
	public static int totalCorrect() {
		int totalRight = 0;
		
		for(int i = 0;i < 20; i++) {
			if(correctAnswers[i] == studentAnswers[i]) {
				totalRight++;
			}
		}
		return totalRight;
	}
	
	//determine total wrong 
	public static int totalWrong() {
		
		int totalWrong = 0;
		
		for(int i = 0; i < correctAnswers.length; i++) {
			if(correctAnswers[i] != studentAnswers[i]) {
				totalWrong++;
			}
		}
		return totalWrong;
	}
	
	//return numbers of the questions missed
	public static int numsMissed(int totalMissed) {
		int counter = 0;
		int[] questionsMissed = new int[totalMissed];
		
		for(int i = 0; i < correctAnswers.length; i++) {
			if(correctAnswers[i] != studentAnswers[i]) {
				questionsMissed[counter] = (i + 1);
				counter++;
			}
		}
		
		for(int i = 0; i < questionsMissed.length; i++) {
			System.out.println("#" + questionsMissed[i]);
		}
		
		return totalMissed;
	}
	
	
}
