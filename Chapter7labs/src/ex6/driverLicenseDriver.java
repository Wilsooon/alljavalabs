package ex6;

import java.util.Scanner;

public class driverLicenseDriver {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter A,B,C or D for all questions");
		char[] answers = new char[20];
		
		for(int i = 0; i < 20; i++) {
			System.out.println("Question #" + (i+1));
			char input = sc.next().charAt(0);	
			if(input == 'A' || input == 'B' || input == 'C' || input == 'D') {
				answers[i] = input;
			}else {
				System.out.println("please enter valid answer");
				i--;
				continue;
				
			}
			
		}
		
		driverLicense.setStudentAnswers(answers);
		
		//start grading
		System.out.println("Total answers correct : " + driverLicense.totalCorrect());
		System.out.println("Total answers wrong : " + driverLicense.totalWrong());
		System.out.println("-----------------");
		System.out.println("Questions missed:");
		driverLicense.numsMissed(driverLicense.totalWrong());
		
		sc.close();
	}

}
