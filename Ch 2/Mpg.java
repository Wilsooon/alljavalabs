import java.util.Scanner;
public class Mpg
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        System.out.println("how many miles?");
        int miles = scan.nextInt();
        System.out.println("how many gallons?");
        int gallons = scan.nextInt();
        
        scan.close();
        
        
        System.out.println("you're getting " + (miles / gallons) + " miles per gallon");
        
    }
}