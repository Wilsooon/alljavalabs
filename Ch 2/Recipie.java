import java.util.Scanner;
public class Recipie
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("How many cookies are you making?");
        double numOfCookies = scan.nextInt();
        double rate = numOfCookies / 48;
        scan.close();
        
        System.out.println("You will need :" + rate * 1.5 + "Cups of sugar");
        System.out.println("You will need :" + rate * 1 + "Cups of butter");
        System.out.println("You will need :" + rate * 2.75 + "Cups of flour");
        
    }
}