import java.util.Scanner;
public class Average
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Score of Test #1 :");
        int score1 = scan.nextInt();
        System.out.println("Score of Test #2 :");
        int score2 = scan.nextInt();
        System.out.println("Score of Test #3 :");
        int score3 = scan.nextInt();
        
        scan.close();
        
        
        System.out.println("your test average is " + (score1 + score2 + score3) / 3 + "!");
        
    }
}