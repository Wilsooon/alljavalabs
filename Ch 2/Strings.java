import java.util.Scanner;
public class Strings
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        System.out.println("What is your favorite city?");
        String city = scan.next();
        scan.close();
        
        
        System.out.println(city.length() + " letters \n"
                          + city.toUpperCase() + "\n"
                          + city.toLowerCase() + "\n"
                          + city.charAt(0) + " is the first letter");
        
    }
}