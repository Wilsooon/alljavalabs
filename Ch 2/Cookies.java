import java.util.Scanner; 

public class Cookies
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        System.out.println("How many cookies did you eat?");
        int x = scan.nextInt();
        scan.close();
        
        System.out.println(x + " cookies = " + x * 75 + " calories");
    }
}