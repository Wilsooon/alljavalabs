import java.util.Scanner;
public class Class
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        System.out.println("how many males are there?");
        double males = scan.nextInt();
        System.out.println("how many females are there?");
        double females = scan.nextInt();
        
        scan.close();
        
        double total = males + females;
        double mPercent = males / total * 100;
        double fPercent = females / total * 100;
        
        System.out.println("Males : " + mPercent + "\n"
                          + "Females : " + fPercent);
        
    }
}