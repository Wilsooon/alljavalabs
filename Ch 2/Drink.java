import java.util.Scanner;
public class Drink
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        double numOfPeople = 15000;
        double drinkers = .18 * numOfPeople;
        double citrus = .58 * drinkers;
        
        System.out.println("people who drink one or more drinks a week : " + drinkers);
        System.out.println("people who prefer citrus : " + citrus);
    }
}