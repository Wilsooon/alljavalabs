package ex6;


public class Lab6EmployeeDriver {
	
	//default 
	Lab6Employee emp1 = new Lab6Employee();

	//2 arg
	Lab6Employee emp2 = new Lab6Employee("Austin", 85451);

	//4 arg
	Lab6Employee emp3 = new Lab6Employee("Austin", 984524, "IT", "software Engineer");
	
	
}
