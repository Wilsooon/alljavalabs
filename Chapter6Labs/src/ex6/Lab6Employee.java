package ex6;


public class Lab6Employee
{
    //Fields
    private String name;        
    private int idNumber;       
    private String department;  
    private String position;    

    public Lab6Employee(String name, int id, String dept, String position)
    {
        this.name = name;
        this.idNumber = id;
        this.department = dept;
        this.position = position;
    }

    public Lab6Employee(String name, int id)
    {
        this.name = name;
        this.idNumber = id;
    }

    public Lab6Employee(){
        this.name = "";
        this.idNumber = 0;
        this.department = "";
        this.position = "";
    }

    // set the Lab6Employee's Name
    public void setName(String empName)
    {
        name = empName;
    }

    // set the Lab6Employee's ID Number
    public void setID(int empID)
    {
        idNumber = empID;
    }

    // set the department where the EMployee works
    public void setDept(String dept)
    {
        department = dept;
    }

    // set the position the Lab6Employee holds
    public void setPost(String post)
    {
        position = post;
    }

    // return Lab6Employee's name
    public String getName()
    {
        return name;
    }

    // return Lab6Employee's ID Number
    public int getID()
    {
        return idNumber;
    }

    // return the department where the Lab6Employee works
    public String getDept()
    {
        return department;
    }

    // return the position the Lab6Employee holds
    public String getPost()
    {
        return position;
    }
}