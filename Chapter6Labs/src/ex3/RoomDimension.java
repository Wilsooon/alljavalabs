package ex3;

public class RoomDimension {
	
	private double width;
	private double length;
	
	public RoomDimension(double width, double length) {
		this.width = width;
		this.length = length;
	}
	
	public double getWidth() {
		return width;
	}
	public double getLength() {
		return length;
	}
	
	public double calcArea() {
		double area = this.width * this.length;
		return area;
	}
	
	@Override
    public String toString() {
        return "length= " + length + ", width= " + width;
    }

	

}
