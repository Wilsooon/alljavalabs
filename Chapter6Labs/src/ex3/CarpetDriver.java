package ex3;

public class CarpetDriver {
	
	public static void main(String[] args) {
		 final double CARPET_PRICE = 8.0;
		
		RoomDimension room1 = new RoomDimension(12, 10);
		RoomCarpet carpet1 = new RoomCarpet(room1, CARPET_PRICE);
		
		System.out.println(carpet1);
		
	}

}