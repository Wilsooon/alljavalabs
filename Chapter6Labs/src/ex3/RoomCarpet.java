package ex3;

public class RoomCarpet {
	
	private RoomDimension size;
	private double cost;
	
	
	public RoomCarpet(RoomDimension size, double cost) {
		this.size = size;
		this.cost = cost;
	}
	
	public double calcCost() {
		double finalCost = cost * size.calcArea();
		return finalCost;
	}
		
	 @Override
     public String toString() {
         return "roomDimensions= " + size + "\n"
                 + " costOfCarpet= " + cost + ", "
                         + "total cost= " + calcCost();
     }

}
