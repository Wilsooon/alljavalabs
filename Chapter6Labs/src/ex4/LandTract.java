package ex4;

public class LandTract {
	
	private double width;
	private double length;
	
	public LandTract(double width, double length) {
		this.width = width;
		this.length = length;
		
	}
	
	public double calcArea() {
		return width * length;
	}
	
	public boolean equals(LandTract tract) {
		if(this.length == tract.length && this.width == tract.width) {
			return true;
		}else {
			return false;
		}
		
	}
	
	@Override
    public String toString() {
        return "Length is : " + length + "\n"
        		+ "Width is : " + width + "\n"
        		+ "for an area of : " + calcArea();
    }

}
