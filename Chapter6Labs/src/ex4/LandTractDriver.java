package ex4;

import java.util.Scanner;

public class LandTractDriver {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("How long is your land?");
		double l1 = sc.nextDouble();
		System.out.println("How wide is your land?");
		double w1 = sc.nextDouble();
		
		System.out.println("How long is your 2nd piece of land?");
		double l2 = sc.nextDouble();
		System.out.println("How wide is your 2nd piece of land?");
		double w2 = sc.nextDouble();
		
		LandTract land1 = new LandTract(l1, w1);
		LandTract land2 = new LandTract(l2, w2);
		
		
		if(land1.equals(land2)) {
			System.out.println("They are equal size!");
		}else {
			System.out.println(land1.toString());
			System.out.println("-------------------------");
			System.out.println(land2.toString());
		}
		
		sc.close();
		
	}	

}
