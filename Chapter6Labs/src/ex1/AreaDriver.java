package ex1;
public class AreaDriver {

	public static void main(String[] args) {
		
		//circle
	    System.out.printf("a circle with a " 
		+ "radius of 35.0 is %6.2f\n", Area.calcArea(35.0));

	    
	    //rectanglee
	    System.out.printf("a rectangle with a "
        + "length of 5 and a width of 50 is %6.2f\n", Area.calcArea(5, 50));

	    
	    //cylinder
	    System.out.printf("a cylinder with a "
        + "radius of 18.0 and a height " + "of 24.0 is %6.2f\n", Area.calcArea(18.0, 24.0));


	}

}
