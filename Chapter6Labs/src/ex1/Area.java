package ex1;

public class Area {
	
	
	//circle
	public static double calcArea(double radius) {
        return Math.PI * radius * radius;
    }
	
	//rectangle
	public static double calcArea(int length, int width) {
        return length * width;
    }
	
	//cylinder
	public static double calcArea(double radius, double height) {
        return Math.PI * radius * radius * height;
    }

}
