package ex5;

public class Month {
	
	private int monthNumber;

	    public Month()
	    {
	        this.monthNumber = 1;
	    }

	    public Month(int monthNumber)
	    {
	        if (monthNumber < 1 || monthNumber > 12)
	        {
	            this.monthNumber = 1;
	        }
	        else
	        {
	            this.monthNumber = monthNumber;
	        }
	    }

	    public Month(String monthName)
	    {
	        monthName = monthName.toLowerCase();
	        switch (monthName)
	        {
	            case "january":
	                monthNumber = 1;
	                break;
	            case "february":
	                monthNumber = 2;
	                break;
	            case "march":
	                monthNumber = 3;
	                break;
	            case "april":
	                monthNumber = 4;
	                break;
	            case "may":
	                monthNumber = 5;
	                break;
	            case "june":
	                monthNumber = 6;
	                break;
	            case "july":
	                monthNumber = 7;
	                break;
	            case "august":
	                monthNumber = 8;
	                break;
	            case "september":
	                monthNumber = 9;
	                break;
	            case "october":
	                monthNumber = 10;
	                break;
	            case "november":
	                monthNumber = 11;
	                break;
	            case "december":
	                monthNumber = 12;
	                break;
	            default:
	                monthNumber = 1;
	        }
	    }

	    public void setMonthNumber(int monthNumber)
	    {
	        if (monthNumber < 1 || monthNumber > 12)
	        {
	            this.monthNumber = 1;
	        }
	        else
	        {
	            this.monthNumber = monthNumber;
	        }
	    }

	    public int getMonthNumber()
	    {
	        return monthNumber;
	    }

	    public String getMonthName()
	    {
	        String monthName;
	        switch (monthNumber)
	        {
	            case 1:
	                monthName = "January";
	                break;
	            case 2:
	                monthName = "February";
	                break;
	            case 3:
	                monthName = "March";
	                break;
	            case 4:
	                monthName = "April";
	                break;
	            case 5:
	                monthName = "May";
	                break;
	            case 6:
	                monthName = "June";
	                break;
	            case 7:
	                monthName = "July";
	                break;
	            case 8:
	                monthName = "August";
	                break;
	            case 9:
	                monthName = "September";
	                break;
	            case 10:
	                monthName = "October";
	                break;
	            case 11:
	                monthName = "November";
	                break;
	            case 12:
	                monthName = "December";
	                break;
	            default:
	                monthName = "OUCH!, out of range";
	        }
	        return monthName;
	    }

	    public String toString()
	    {
	        String str = "The month of the number you entered is: " + getMonthName();
	        return str;
	    }

	    public boolean equals(Month object2)
	    {
	        boolean status;
	        if (getMonthNumber() == object2.getMonthNumber() && getMonthName().equals(object2.getMonthName()))
	        {
	            status = true;
	        }
	        else
	        {
	            status = false;
	        }
	        return status;
	    }

	    public boolean greaterThan(Month object2)
	    {
	        boolean status;
	        if (getMonthNumber() > object2.getMonthNumber())
	        {
	            status = true;
	        }
	        else
	        {
	            status = false;
	        }
	        return status;
	    }

	    public boolean lessThan(Month object2)
	    {
	        boolean status;
	        if (getMonthNumber() < object2.getMonthNumber())
	        {
	            status = true;
	        }
	        else
	        {
	            status = false;
	        }
	        return status;
	    }
	}