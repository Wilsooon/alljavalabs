package ex5;

import java.util.Scanner;

public class MonthDriver {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter a month number : ");
		int month = sc.nextInt();
		
		Month newmonth = new Month(month);
		
		System.out.println(newmonth.getMonthName());
		sc.close();
		
	}

}
