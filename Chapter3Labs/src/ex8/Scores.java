package ex8;

public class Scores {
	
	private double score1;
	private double score2;
	private double score3;
	
	//setters
	public void setScore1(double score1) {
		this.score1 = score1;
	}
	public void setScore2(double score2) {
		this.score2 = score2;
	}
	public void setScore3(double score3) {
		this.score3 = score3;
	}
	
	//average
	public double getAvg() {
		double avg = ((score1 + score2) + score3) / 3;
		return avg;
	}
	
	
	
}
