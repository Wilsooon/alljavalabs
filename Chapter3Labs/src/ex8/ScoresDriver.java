package ex8;
import java.util.Scanner;


public class ScoresDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		Scores score1 = new Scores();
		
		System.out.println("What is the first score?");
		score1.setScore1(sc.nextDouble());
		System.out.println("What is the second score?");
		score1.setScore2(sc.nextDouble());
		System.out.println("What is the third score?");
		score1.setScore3(sc.nextDouble());
		
		//display
		System.out.println("The average is " + score1.getAvg());
		sc.close();
	}

}
