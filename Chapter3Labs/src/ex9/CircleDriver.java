package ex9;
import java.util.Scanner;

public class CircleDriver {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Circle newCircle = new Circle();
		
		System.out.println("What is the radius of the circle");
		newCircle.setRadius(sc.nextDouble());
		
		//display
		
		System.out.println("the area is  " + newCircle.getArea());
		System.out.println("the diameter is " + newCircle.getDiameter());
		System.out.println("the circumference is " + newCircle.getCirc());
		
		sc.close();
	}
	
}
