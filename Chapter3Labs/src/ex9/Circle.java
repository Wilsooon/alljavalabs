package ex9;

public class Circle {
	
	private double radius;
	static final double pi = 3.14159;
	
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public double getArea() {
		double area  = pi * (radius * radius);
		return area;
	}
	
	public double getDiameter() {
		double diameter = radius * 2;
		return diameter;
	}
	
	public double getCirc() {
		double circ = 2 * (pi * radius);
		return circ;
	}
	
	

}
