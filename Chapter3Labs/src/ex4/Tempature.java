package ex4;

public class Tempature {

	private double ftemp;
	
	public Tempature(double ftemp) {
		this.ftemp = ftemp;
	}
	
	public void setFahrenheit(double Fahrenheit) {
		ftemp = Fahrenheit;
	}
	
	public double  getFahrenheit() {
		return ftemp;
	}
	
	public double getCelsius() {
		double celsius = (5.0 / 9) * (ftemp - 32);
		return celsius;
	}
	
	public double getKelvin() {
		double kelvin = ((5.0 / 9) * (ftemp - 32)) + 273;
		return kelvin;
	}
	
}
