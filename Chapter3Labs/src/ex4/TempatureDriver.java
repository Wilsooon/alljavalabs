package ex4;
import java.util.Scanner;

public class TempatureDriver {
	
	public static void main(String[] args) {
		

		Scanner sc = new Scanner(System.in);
		System.out.println("Whats the tempature you wish to convert?");
		Tempature newTemp = new Tempature(sc.nextDouble());
		sc.close();
		
		//display
		
		System.out.println("The temp is : " + newTemp.getFahrenheit());
		System.out.println("The temp in celsius is : " + newTemp.getCelsius());
		System.out.println("The temp in Kelvin is : " + newTemp.getKelvin());
		
	}
}
