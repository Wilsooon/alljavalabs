package ex6;

public class Payroll {
	
	private int idNum;
	private String name;
	private double rate;
	private double hours;
	
	//constructor
	public Payroll(int id, String name) {
		idNum = id;
		this.name = name;
	}
	
	//setters
	public void setRate(double rate) {
		this.rate = rate;
	}
	public void setHours(double hours) {
		this.hours = hours;
	}
	
	//getters
	public int getId() {
		return idNum;
	}
	public String getName() {
		return name;
	}
	public double getRate() {
		return rate;
	}
	public double getHours() {
		return hours;
	}
	
	
	public double grossIncome() {
		return(rate * hours);
	}

}
