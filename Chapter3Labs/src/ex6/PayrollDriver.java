package ex6;
import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("what is your id?");
		int id  = sc.nextInt();
		sc.nextLine();
		System.out.println("What is your name?");
		String name = sc.nextLine();
		
		
		//constructor
		Payroll emp1 = new Payroll(id, name);
		
		//setters
		System.out.println("How many hours did you work?");
		double hours = sc.nextDouble();
		emp1.setHours(hours);
		System.out.println("How much do you make per hour?");
		double rate = sc.nextDouble();
		emp1.setRate(rate);
		
		
		//display
		System.out.println(emp1.getName());
		System.out.println(emp1.getId());
		System.out.println(emp1.getHours());
		System.out.println(emp1.getRate());
		System.out.println(emp1.grossIncome());
		
		sc.close();
		
		
	}

}
