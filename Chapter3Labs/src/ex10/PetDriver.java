package ex10;
import java.util.Scanner;
public class PetDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		Pet myPet = new Pet();
		
		System.out.println("What is your pets name?");
		myPet.setName(sc.next());
		System.out.println("What is your pets age?");
		myPet.setAge(sc.nextInt());
		System.out.println("What kind of pet?");
		myPet.setType(sc.next());
		
		
		//display
		System.out.println("");
		System.out.println("");
		System.out.println(myPet.getName());
		System.out.println(myPet.getType());
		System.out.println(myPet.getAge());

		sc.close();
	}

}
