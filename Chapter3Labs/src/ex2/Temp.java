package ex2;

public class Temp {
	
	private int year;
	private String make;
	private int speed;
	
	public Temp (int carYear, String carMake) {
		year = carYear;
		make = carMake;
		speed = 0;
	}
	
	//accessors
	
	public int getYear() {
		return year;
	}
	public String getMake() {
		return make;
	}
	public int getspeed() {
		return speed;
	}
	
	public int accelerate() {
		speed += 5;
		return speed;
	}
	
	public int brake() {
		speed -= 5;
		return speed;
	}

}
