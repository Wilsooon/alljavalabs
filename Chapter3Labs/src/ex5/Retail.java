package ex5;

public class Retail {
	
	private String desc;
	private int inventory;
	private double price;
	
	
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public void setInventory(int inventory) {
		this.inventory = inventory;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	
	public String getDesc() {
		return desc;
	}
	public int getInventory() {
		return inventory;
	}
	public double getPrice() {
		return price;
	}
	
	
}
