package ex3;

public class TempDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Temp me = new Temp();
		Temp friend1 = new Temp();
		Temp friend2 = new Temp();
		
		me.setName("Austin");
		me.setAge(20);
		me.setAddress("6166 state rd. DD");
		me.setNumber("636-535-8279");
		
		friend1.setName("Zach");
		friend1.setAge(20);
		friend1.setAddress("Ranken");
		friend1.setNumber("123456789");
		
		friend2.setName("Micheal");
		friend2.setAge(21);
		friend2.setAddress("Ranken");
		friend2.setNumber("123456789");
		
		
		//display
		
		System.out.println(me.getName());
		System.out.println(me.getAge());
		System.out.println(me.getAddress());
		System.out.println(me.getNumber());
		
		System.out.println("");
		
		System.out.println(friend1.getName());
		System.out.println(friend1.getAge());
		System.out.println(friend1.getAddress());
		System.out.println(friend1.getNumber());
		
		System.out.println("");
		
		System.out.println(friend2.getName());
		System.out.println(friend2.getAge());
		System.out.println(friend2.getAddress());
		System.out.println(friend2.getNumber());
		
		
	}

}
