package ex3;

public class Temp {
	
	private String name;
	private int age;
	private String address;
	private String number;
	
	//setters
	
	public void setName(String name) {
		this.name = name;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	
	// getters
	
	public String getName() {
		return name;
	}
	public int getAge() {
		return age;
	}
	public String getAddress() {
		return address;
	}
	public String getNumber() {
		return number;
	}
	
	
	

}
