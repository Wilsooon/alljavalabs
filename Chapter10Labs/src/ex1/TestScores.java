package ex1;

public class TestScores {
	
	private double[] scores;

    public TestScores(double[] scores) {
        for(int i = 0; i < scores.length; i++) {
            if(scores[i] < 0 || scores[i] > 100) {
            IllegalArgumentException e = new IllegalArgumentException();
            throw e;
            }
        }
            this.scores = scores;
    }
    public double getAverage(){
        double average = 0;

        for(int i = 0; i < scores.length;i++) {
            average += scores[i];
        }
        return average / scores.length;

    }

}
