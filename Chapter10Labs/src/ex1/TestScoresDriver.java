package ex1;

public class TestScoresDriver {

	public static void main(String[] args) {
		
		double[] testScores = {89,56,98,74,49,85,63,92,74, -1};

        try {
            TestScores test1 = new TestScores(testScores);
            System.out.println(test1.getAverage());
        }
        catch(IllegalArgumentException e) {
                System.out.println("Please enter a vaild test score!");
        }
		
	}

}
