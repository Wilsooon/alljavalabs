import java.util.Scanner;
import java.text.NumberFormat;

public class FastFood
{
    public static void main(String[] args)
    {
        // hamburgers - 1.25
        // cheeseburger - 1.50
        // sodas - 1.95
        // fries - .95
        
        //collect info
        Scanner scan = new Scanner(System.in);
        NumberFormat mon = NumberFormat.getCurrencyInstance();
        
	// how many hamburgers
        System.out.println("How many Hamburgers?");
        int numberOfHamburgers = scan.nextInt();
        
	// how many cheeseburgers
        System.out.println("How many Cheeseburgers?");
        int numberOfChesseburgers = scan.nextInt();
        
	// how many sodas
        System.out.println("How many sodas?");
        int numberOfSodas = scan.nextInt();
        
	//how many sodas
        System.out.println("How many fries?");
        int numberOfFries = scan.nextInt();
        
	//what is your name
        System.out.println("Whats your name?");
        String name = scan.next();
        
        scan.close();
        
	//calculate total cost
        double total = (numberOfHamburgers * 1.25) + (numberOfChesseburgers * 1.50) + (numberOfSodas * 1.95) + (numberOfFries * .95);
        
        //display info
        System.out.println("\n");
        System.out.println("\n");
        System.out.println(mon.format(total));
        System.out.println(name.toUpperCase());
        
        
        
    }
}