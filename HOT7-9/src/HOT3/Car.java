package HOT3;

public class Car extends Vehicle {
	
	//overriding the accelerate method
	@Override
	public void accelerate() {
		super.speed += 10;
	}

}
