package HOT3;

public class Vehicle {
	
	protected int speed = 0;
	
	//getter for speed
	public int getSpeed() {
		return speed;
	}

	//adds 5 to speed when called
	void accelerate() {
		this.speed += 5;
	}
	
	public static void main(String[] args) {
		
		//creating car and truck objects
		Vehicle car = new Car();
		Vehicle truck = new Truck();
		
		//calling accelerate methods for both car and truck and displaying their speed
		car.accelerate();
		car.accelerate();
		car.accelerate();
		System.out.println("Car is accelerating current speed is : " + car.getSpeed());
		truck.accelerate();
		truck.accelerate();
		truck.accelerate();
		System.out.println("Truck is accelerating current speed is : " + truck.getSpeed());
		
		
	}
	

}
