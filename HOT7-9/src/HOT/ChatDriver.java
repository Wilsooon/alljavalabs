package HOT;

import java.util.Scanner;

public class ChatDriver {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		// creating parallel arrays
		int[] areaCode = {262, 414, 608, 715, 815, 920};
		double[] rate = {.07,.10,.05,.16,.24,.14};
		
		//variables
		int areaCodeInput;
		int callLength;
		int position = 0;
		double cost;
		
		//collecting info
		System.out.println("please enter area code :");
		areaCodeInput = sc.nextInt();
		System.out.println("How long will this call be ?");
		callLength = sc.nextInt();
		
		//finding the position of area code
		for(int i = 0; i < areaCode.length; i++) {
			if(areaCodeInput == areaCode[i]) {
				position = i;
			}else {
				continue;
			}
		}
		
		// figure cost and display info
		cost = (rate[position] * callLength);
		System.out.println("For a call " + callLength + " minutes long \n" 
		+ "at " + rate[position] + " per minute will cost you : $" + cost);
		sc.close();

	}

}
