package HOT2;

public class GameDriver {

	public static void main(String[] args) {
	
		//instantiate new object of Game
		Game game1 = new Game();
		game1.setNameOfGame("FootBall");
		game1.setNumOfPlayers(24);
		
		//instantiate new object of GameWithTimeLimit
		GameWithTimeLimit gwtl = new GameWithTimeLimit();
		gwtl.setGameTime(120);
		
		//calling tostring of the classes
		System.out.println(game1.toString());
		System.out.println(gwtl.toString());

	}

}
