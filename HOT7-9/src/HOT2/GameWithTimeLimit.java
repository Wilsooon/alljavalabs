package HOT2;

public class GameWithTimeLimit extends Game {
	
	//fields
	private int gameTime;
	
	
	//getters and setters
	public int getGameTime() {
		return gameTime;
	}
	public void setGameTime(int gameTime) {
		this.gameTime = gameTime;
	}
	
	//to string
    @Override
    public String toString()
    {
        return " Game Time : " + gameTime;
	
    }
}