package HOT2;

public class Game {
	
	//fields
	private String nameOfGame;
	private int numOfPlayers;
	
	
	//getters and setters
	public String getNameOfGame() {
		return nameOfGame;
	}
	public void setNameOfGame(String nameOfGame) {
		this.nameOfGame = nameOfGame;
	}
	public int getNumOfPlayers() {
		return numOfPlayers;
	}
	public void setNumOfPlayers(int numOfPlayers) {
		this.numOfPlayers = numOfPlayers;
	}
	
	//to string
    @Override
    public String toString()
    {
    	String str = " Game name : " + nameOfGame + "\n"
    			+ " Number of Players : " + numOfPlayers;
        return str;
    }
}
