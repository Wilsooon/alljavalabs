package HOT3;

public class Hot3 {
	
	//overload 2 nums
	public static int addNums(int num1, int num2) {
		return num1 + num2;
	}
	// overload 3 nums
	public static int addNums(int num1, int num2, int num3) {
		return num1 + num2 + num3;
	}

}
