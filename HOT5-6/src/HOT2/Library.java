package HOT2;

public class Library {
	
	private Book book1;
	private String name;
	
	//2 param constructor
	public Library(Book book1, String name) {
		this.book1 = book1;
		this.name = name;
	}
	
	//to string
	@Override
    public String toString() {
        return "Name : " + name + "\n"
        		+ "Book info" + "\n" 
        		+ book1.toString();
    }

}
