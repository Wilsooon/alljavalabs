package HOT2;

public class LibraryDriver {

	public static void main(String[] args) {
		Book book1 = new Book("The Hatchet", "Gary Pulsen", "1/2/3");
		Library library1 = new Library(book1, "Ranken Library");
		
		System.out.println(library1.toString());

	}

}
