package HOT2;

public class Book {
	
	private String name;
	private String date;
	private String author;
	
	//constructor
	public Book(String name, String date, String author) {
		this.name = name;
		this.date = date;
		this.author = author;
	}

	//getters
	public String getName() {
		return name;
	}
	public String getDate() {
		return date;
	}
	public String getAuthor() {
		return author;
	}
	
	//to string
	@Override
    public String toString() {
        return "Name : " + name + "\n"
        		+ "Date : " + date + "\n"
        		+ "Author : " + author;
    }

}
