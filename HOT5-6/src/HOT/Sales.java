package HOT;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Sales {
	
	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(System.in);
		PrintWriter print = new PrintWriter("WeeklySales.txt");
		
		int count = 0;
		double total = 0;
		double input = 0;
		
		//for loop asking for input
		for(int i = 0; i < 5;) {
			System.out.println("Enter amount of sales :");
			input = sc.nextDouble();
			
			if(input >= 0) {
				print.println(input);
				count++;
				i++;
				total += input;
			}else {
				System.out.println("please enter positive number");
				continue;
			}
		}
		
		//display output and write to file
		System.out.println("Thank You!");
		print.println("Number of loop cycles :" + count);
		print.println("Total Sales : " + total);
		
		//close scanner and file
		sc.close();
		print.close();
		
	}
	
}
