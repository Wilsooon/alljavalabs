package ex2;
import java.util.Scanner;
public class Time {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter amount of seconds :");
		int sec = sc.nextInt();
		int days;
		int hours;
		int minutes;
		
		if(sec >= 84600) {
			days = (sec / 84600);
			System.out.println(days + " Days");
			hours = (sec % 84600) / 3600;
			System.out.println(hours + " Hours");
			minutes = (sec % 3600) / 60;
			System.out.println(minutes + " Minutes");
		}else if(sec >= 3600){
			hours = (sec/ 3600);
			System.out.println(hours + " Hours");
			minutes = (sec % 3600) / 60;
			System.out.println(minutes + " Minutes");
		}else {
			minutes = (sec / 60);
			System.out.println(minutes + " Minutes");
		}
		sc.close();
	}

}
