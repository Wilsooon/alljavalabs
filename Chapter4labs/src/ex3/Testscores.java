package ex3;

public class Testscores {
	
	private double score1;
	private double score2;
	private double score3;
	
	//setters
	public void setScore1(double score1) {
		this.score1 = score1;
	}
	public void setScore2(double score2) {
		this.score2 = score2;
	}
	public void setScore3(double score3) {
		this.score3 = score3;
	}
	
	//average
	public String getAvg() {
		double avg = ((score1 + score2) + score3) / 3;
		String grade;
		
		if(avg <= 100 && avg >= 90) {
			grade = "A";
		}else if(avg < 90 && avg >= 80) {
			grade = "B";
		}else if(avg < 80 && avg >= 70) {
			grade = "C";
		}else if(avg < 70 && avg >= 60) {
			grade = "D";
		}else {
			grade = "F";
		}
		
		return grade;
	}

}
