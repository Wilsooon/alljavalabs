package ex5;

public class Bank {
	
	public int checks;
	public double total;
	
	
	public void setChecks(int checks) {
		this.checks = checks;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
	public void setFees() {
		if(checks > 20){
			total -= .10 * checks + 10;
			if(total < 400)
				total -= 15;
			
		}else if(checks > 20 && checks <= 39) {
			total -= .08 * checks + 10;
			if(total < 400)
				total -= 15;
		}else if(checks > 40 && checks <= 59) {
			total -= .06 * checks + 10;
			if(total < 400)
				total -= 15;
		}else {
			total -= .04 * checks +10;
			if(total < 400)
				total -= 15;
		}

	}
	
	public double getTotal() {
		
		return total;
	}
	
}
