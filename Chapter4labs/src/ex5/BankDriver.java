package ex5;

import java.util.Scanner;

public class BankDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		Bank account1 = new Bank();
		
		System.out.println("how many checks did you write this month?");
		account1.setChecks(sc.nextInt());
		System.out.println("how much is in your account ?");
		account1.setTotal(sc.nextDouble());
		account1.setFees();
		sc.close();
		
		System.out.println("Your current balance is " 
		+ account1.getTotal() + " including account and check fees");
		
	}
	
}
