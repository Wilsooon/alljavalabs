package ex1;
import java.util.Scanner;

public class NumeralsDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		
		int num;
		System.out.println("Enter a number between 1 and 10");
		num = sc.nextInt();
		sc.close();
		
		switch(num) {
			case 1:
				System.out.println("You entered : I");
				break;
			case 2:
				System.out.println("You entered : II");
				break;
			case 3:
				System.out.println("You entered : III");
				break;
			case 4:
				System.out.println("You entered : IV");
				break;
			case 5:
				System.out.println("You entered : V");
				break;
			case 6:
				System.out.println("You entered : VI");
				break;
			case 7:
				System.out.println("You entered : VII");
				break;
			case 8:
				System.out.println("You entered : VIII");
				break;
			case 9:
				System.out.println("You entered : IX");
				break;
			case 10:
				System.out.println("You entered : X");
				break;
			default:
				System.out.println("please pick a number between 1 and 10");
				break;
		}
	}

}
