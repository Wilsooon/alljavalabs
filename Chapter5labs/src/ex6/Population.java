package ex6;

public class Population {

	private int population;
	private double growth;
	private int days;
	
	
	// setters
	public void setPopulation(int population) {
		this.population = population;
	}
	public void setGrowth(double growth) {
		this.growth = growth;
	}
	public void setDays(int days) {
		this.days = days;
	}


	//loop to display info
	public void displayGrowth() {
		
		for(int i = 1; i <= days; i++) {
			population += (growth * population);
			System.out.println("Day:" + i  + "    population = " + population);
		}
		
	}
	
}
