package ex6;

import java.util.Scanner;

public class PopulationDriver {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Population pop1 = new Population();
		
		int days;
		int startingpop;
		double growth;
		
		//collect and set info
		System.out.println("What is the starting population?");
		startingpop = sc.nextInt();
		if(startingpop > 1) {
			pop1.setPopulation(startingpop);
		}else {
			System.out.println("Please enter valid number");
		}
		System.out.println("How long can the population grow?");
		days = sc.nextInt();
		if(days > 0) {
			pop1.setDays(days);
		}else {
			System.out.println("Please enter valid number");
		}
		System.out.println("What is the growth rate?");
		growth = sc.nextDouble();
		if(growth > 0) {
			pop1.setGrowth(growth);
		}else {
			System.out.println("Please enter valid number");
		}
		pop1.displayGrowth();
		sc.close();
		

	}

}
