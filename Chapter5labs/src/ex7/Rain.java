package ex7;

import java.util.Scanner;

public class Rain {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		
		int totalinches = 0;
		int months = 0;
		int inches;
		
		System.out.println("How many years?");
		int years = sc.nextInt();
		if(years >= 1) {
			
			//loop for years
			for(int i = 1; i <= years; i++) {
				
				//loop for months
				for(int m = 1; m <= 12; m++) {
					System.out.println("how many inches or rainfall?");
					inches = sc.nextInt();
					if(inches > 0) {
						months += 1;
						totalinches += inches;
					}
				}
				
			}
			System.out.println("Total months : " + months);
			System.out.println("total inches : " + totalinches);
			System.out.println("Avg rainfall per month : " + ((double)totalinches / months));
		}
		
		sc.close();
		
		
		
		
	}

}
