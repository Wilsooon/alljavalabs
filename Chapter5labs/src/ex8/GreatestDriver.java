package ex8;

import java.util.Scanner;

public class GreatestDriver {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int min = 0;
		int max = 0;
		int input = 0;
		
		while(input != -99) {
			
			System.out.println("Please enter a number or -99 to end");
			input = sc.nextInt();
			
			if(input > max)
				max = input;
			else if(input < max || input < min) {
				
				if(input != -99)
				min = input;
			}
				
		}
		
		System.out.println("Highest number = " + max + "\n" 
				+ "Lowest Number = " + min);
		sc.close();

	}

}
