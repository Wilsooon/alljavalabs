package ex4;

import java.util.Scanner;


public class Pennies {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		double pennies = .01;
		
		
		System.out.println("How many days did you work?");
		int days = sc.nextInt();
		
		for(int i = 1; i < days; i ++) {
		pennies *= 2;
		System.out.println("Day: "+ i + " you made " + pennies);
		}
		sc.close();

	}

}
