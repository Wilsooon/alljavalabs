package ex10;

public class Saving {
	
	private double rate;
	private double balance;
	
	
	//getters
	public double getRate() {
		return rate;
	}
	public double getBalance() {
		return balance;
	}

	//construct
	public Saving(double rate, double balance) {
		this.rate = rate;
		this.balance = balance;
	}
	
	//withdraw
	public double withdraw(double withdrawAmount) {
		this.balance -= withdrawAmount;
		return balance;
	}
	
	//deposit
	public double deposit(double deposit) {
		this.balance += deposit;
		return balance;
	}
	
	//interest
	public double addInterest() {
		
		this.balance += (balance * (rate / 12));
		return balance;
	}

}
