package ex10;

import java.util.Scanner;

public class SavingDriver {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		double rate;
		double bal;
		int months;
		int deposits = 0;
		int withdraws = 0;
		
		
		System.out.println("What is the starting balance?");
		bal =sc.nextDouble();
		
		System.out.println("What is the rate of the account?");
		rate = sc.nextDouble();
		
		Saving act = new Saving(rate, bal);
		
		System.out.println("how many months since the account opened?");
		months = sc.nextInt();
		
		for(int i = 1; i <= months; i++) {
			
			System.out.println("Month #" + i);
			System.out.println("----------");
			System.out.println("How much will you deposit?");
			act.deposit(sc.nextDouble());
			System.out.println("How much will you withdraw?");
			act.withdraw(sc.nextDouble());
			act.addInterest();
			deposits++;
			withdraws++;
			
		}
		
		System.out.println("Ending balance is  " + act.getBalance());
		System.out.println("You made " + withdraws + " withdraws and " + deposits + " deposits");
		System.out.println("Total interest you earned is :" + (act.getBalance() - bal));
		sc.close();
		
	}

}
