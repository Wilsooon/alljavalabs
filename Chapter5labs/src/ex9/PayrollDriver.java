package ex9;

import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		Payroll emp1 = new Payroll();
		
		int input = 1;
		int vInt;
		double vDouble;
		
		while(input != 0) {
			
			System.out.println("What is your Id number ?");
			vInt = sc.nextInt();
			if(vInt > 0)
				emp1.setIdNum(vInt);
			else
				System.out.println("please enter a positvie number");
			
			System.out.println("What is your gross pay ?");
			vDouble = sc.nextDouble();
			if(vDouble > 0)
				emp1.setGrossPay(vDouble);
			else
				System.out.println("please enter a positvie number");
			
			System.out.println("What is your state tax ?");
			vDouble = sc.nextDouble();
			if(vDouble > 0)
				emp1.setStateTax(vDouble);
			else
				System.out.println("please enter a positvie number");
			
			System.out.println("What is your federal tax ?");
			vDouble = sc.nextDouble();
			if(vDouble > 0)
				emp1.setFederalTax(vDouble);
			else
				System.out.println("please enter a positvie number");
			
			System.out.println("What is your FICA fee's ?");
			vDouble = sc.nextDouble();
			if(vDouble > 0)
				emp1.setFICA(vDouble);
			else
				System.out.println("please enter a positvie number");
			System.out.println("please enter 0 if you are finished.");
			input = sc.nextInt();
			
		}
		
		
		System.out.println("net pay is " + emp1.calcNet() + "\n" + 
				"Gross pay is " + emp1.getGrossPay() + "\n" + 
				"State Tax is " + emp1.getStateTax() + "\n" + 
				"Federal Tax is " + emp1.getFederalTax() + "\n" +
				"FICA Fee's is " + emp1.getFICA());
		
		
		sc.close();
	}

}
