package ex9;

public class Payroll {
	
	private int idNum;
	private double grossPay;
	private double stateTax;
	private double federalTax;
	private double FICA;
	
	
	public int getIdNum() {
		return idNum;
	}
	public void setIdNum(int idNum) {
		this.idNum = idNum;
	}
	public double getGrossPay() {
		return grossPay;
	}
	public void setGrossPay(double grossPay) {
		this.grossPay = grossPay;
	}
	public double getStateTax() {
		return stateTax;
	}
	public void setStateTax(double stateTax) {
		this.stateTax = stateTax;
	}
	public double getFederalTax() {
		return federalTax;
	}
	public void setFederalTax(double federalTax) {
		this.federalTax = federalTax;
	}
	public double getFICA() {
		return FICA;
	}
	public void setFICA(double fICA) {
		FICA = fICA;
	}
	
	
	public double calcNet() {
		
		double net = grossPay - stateTax - federalTax - FICA;
		return net;
	}

}
