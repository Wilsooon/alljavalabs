package ex2;

import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class DistanceDriver {
	
	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner sc = new Scanner(System.in);
		Distance distance = new Distance();
		PrintWriter pw = new PrintWriter("myfile.txt");
		
		System.out.println("How fast are you going?");
		int speed = sc.nextInt();
		
		if(speed > 0) {
			distance.setSpeed(speed);
		}
			
		System.out.println("How long were you traveling?");
		int hours = sc.nextInt();
		sc.close();
		
		if(hours > 0) {
			
			
			pw.println("Hours          Distance");
			pw.println("------------------");
			for(int i = 1; i <= hours; i++) {
				distance.setHours(i);
				pw.println(i + "             " + distance.getDistance());
				
				
			}
			pw.close();
			
		}
		
		
	}
	
	
}
