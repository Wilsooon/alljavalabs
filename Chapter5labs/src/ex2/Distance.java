package ex2;

public class Distance {
	
	private int speed;
	private int hours;
	
	
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public void setHours(int hours) {
		this.hours = hours;
	}
	
	public int getDistance() {
		return(speed * hours);
	}

}
