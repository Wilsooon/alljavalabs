package ex5;

import java.util.Scanner;

public class Hotel {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		
		System.out.println("how many floors are there?");
		int floors = sc.nextInt();
		int totalRooms = 0;
		int usedRooms = 0;
		
		if(floors >= 1) {
			for(int i = 1; i <= floors; i++) {
				int rooms;
				
				System.out.println("how many rooms are on this floor?");
				rooms = sc.nextInt();
				if(rooms >= 10) {
					System.out.println("How many are occupied?");
					usedRooms += sc.nextInt();
					totalRooms += rooms;
				}
			}
			
		}else {
			System.out.println("please enter valid number of floors");
		}
		sc.close();
		
		double rate = (double)usedRooms / totalRooms;
		System.out.println("Total rooms : " + totalRooms);
		System.out.println("Occupied rooms : " + usedRooms);
		System.out.println("Rate : %" + rate * 100);
	}
}
