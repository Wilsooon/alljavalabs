package ex3;

import java.time.DateTimeException;

public class Employee
{
    private String employeeName;
    private String employeeNumber;
    private String employeeHireDate;

    
    //default constructor
    public Employee(){
    	
    }
    
    // constructor
    public Employee(String name, String number, String hireDate)
    {
        this.employeeName = name;
        this.employeeNumber = number;
        this.employeeHireDate = hireDate;
    }
    
    //setters
    public void setEmpName(String employeeName)
    {
        this.employeeName = employeeName;
    }

    public void setEmpNum(String empNum)
    {
        this.employeeNumber = empNum;
    }

    public void setEmpHireDate(String empHireDate)
    {
        this.employeeHireDate = empHireDate;
    }

    //getters
    public String getEmpName()
    {
        return employeeName;
    }

    public String getEmpNum()
    {
        return employeeNumber;
    }

    public String getEmpHireDate()
    {
        return employeeHireDate;
    }


    public String toString()
    {
        return "Employee{" +
                "empName='" + employeeName + '\'' +
                ", empNum='" + employeeNumber + '\'' +
                ", empHireDate='" + employeeHireDate + '\'' +
                '}';
    }
}