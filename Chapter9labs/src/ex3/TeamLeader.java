package ex3;

public class TeamLeader extends ProductionWorker
{
    private String name;
    private final double REQ_TRAIN_HOURS = 36.0;
    private final double MONTH_BONUS = 80.00;
    private double trainingHoursAttended;
    private double hoursWorked;

    
  //default constructor
    public TeamLeader()
    {
    }
    
    //constructor
    public TeamLeader(double hourlyRate, double attendedTrainHours, double hoursWorked)
    {
        super(hourlyRate);
        this.trainingHoursAttended = attendedTrainHours;
        this.hoursWorked = hoursWorked;
    }

    // set name
    public void setName(String name)
    {
        this.name = name;
    }
    // get name
    public String getName()
    {
        return name;
    }

    // set hours work each week
    public void setHoursWorked(double hoursWorked)
    {
        this.hoursWorked = hoursWorked;
    }

    // set attended training hours
    public void setAttendedTrainHours(double attendedTrainHours)
    {
        this.trainingHoursAttended = attendedTrainHours;
    }

    public double getMONTH_BONUS()
    {
        return MONTH_BONUS;
    }

    public double getREQ_TRAIN_HOURS()
    {
        return REQ_TRAIN_HOURS;
    }

    public boolean ifReachedHours()
    {
        boolean reached = false;

        if (trainingHoursAttended >= REQ_TRAIN_HOURS)
        {
            reached = true;
        }
        else
        {
            reached = false;
        }

        return reached;
    }

    public String reached()
    {
        String str;
        if (ifReachedHours())
        {
            str= " reached his hours.";
        }
        else
        {
            str = " did not reach his hours.";
        }
        return str;
    }

    public double calcPay()
    {
        double anualPay;
        double anualBonus = 0;
        double anualPayWithBonus;

        if (ifReachedHours())
        {
            anualBonus = MONTH_BONUS * 12;
        }

        anualPay = getHourlyRate() * (hoursWorked * 251);

        anualPayWithBonus = anualBonus + anualPay;

        return anualPayWithBonus;
    }
}