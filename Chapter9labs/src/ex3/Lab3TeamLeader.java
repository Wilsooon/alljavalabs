package ex3;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Lab3TeamLeader
{
    public static void main(String[] args)
    {
        // initiate scanner for user input
        Scanner input = new Scanner(System.in);

        // initiate Decimal format to format data-types
        DecimalFormat decForm = new DecimalFormat("$#,##0.00");

        TeamLeader leader = new TeamLeader();

        leader.setName("Austin Wilson");

        System.out.println("What is " + leader.getName() + "'s hourly pay rate in 2016?");
        leader.setHourlyRate(input.nextDouble());

        System.out.println("How many hours does " + leader.getName() + " work in a work day?");
        leader.setHoursWorked(input.nextDouble());

        System.out.println("How many hours of Training did " + leader.getName() + " this year?");
        leader.setAttendedTrainHours(input.nextDouble());

        System.out.println(leader.getName() + leader.reached());
        System.out.println("And so, " + leader.getName() + " has an annual pay of "
                + decForm.format(leader.calcPay()));
        
        input.close();
    }
}
