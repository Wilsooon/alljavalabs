package ex3;

public class ProductionWorker extends Employee
{
    private int shift;
    private double hourlyRate;
    String timeODay;

    //constructor with 5 args
    public ProductionWorker(String name, String number, String hireDate, int shift, double hourlyRate)
    {
        super(name, number, hireDate);
        this.shift = shift;
        this.hourlyRate = hourlyRate;
    }

    // default constructor
    public ProductionWorker()
    {
    }

    //constructor with 2 args
    public ProductionWorker(int shift, double hourlyRate)
    {
        this.shift = shift;
        this.hourlyRate = hourlyRate;
    }

    //constructor with 1 arg
    public ProductionWorker(double hourlyRate)
    {
        this.hourlyRate = hourlyRate;
    }

    //setters
    public void setShift(int shift)
    {
        this.shift = shift;
    }

    public void setHourlyRate(double hourlyRate)
    {
        this.hourlyRate = hourlyRate;
    }


    //getters
    public String getShift()
    {


        if (shift == 1)
        {
            timeODay = "Day Shift";
        }
        else if (shift == 2)
        {
            timeODay = "Night Shift";
        }
        else
        {
            timeODay = "ERROR";
        }

        return timeODay;
    }

    public double getHourlyRate()
    {
        return hourlyRate;
    }

    public String toString()
    {
        return super.toString() + "ProductionWorker{" +
                "hourlyRate=" + hourlyRate +
                ", shift=" + timeODay +
                '}';
    }
}