package ex1;

public class Employee {
	
	//private fields
    public String name;
    public String number;
    public String hireDate;
    
    private boolean isValid = false;
	
	
    //constructor
    public Employee(String n, String num, String hd)
    {
        name = n;
        number = num;
        hireDate = hd;
    }
    
    //get name
    public String getName()
    {
        return name;
    }
    
    //get hireDate
    public String getHireDate()
    {
        return hireDate;
    }
    
    //to string
    public String toString()
    {
        String string1 = "Name: " +name+ "\nEmployee Number: ";
         
        if(number == "")
        {
            string1+="Invalid Employee Number";
        }
        else
        {
            string1+=number;
        }
        string1 +=("\nHire Date: " +hireDate);
        return string1;
    }
	

}
