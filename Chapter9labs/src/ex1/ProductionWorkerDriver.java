package ex1;

import java.util.Scanner;
import java.util.*;
import java.text.DecimalFormat;
import java.text.DateFormat;

public class ProductionWorkerDriver {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		String name; String n;
        String number; String num;
        String hireDate; String hd;
        int shift;
        double payRate; double rate;
		
		//gather info
        System.out.println("Enter your name: ");
        name = s.nextLine();
         
        System.out.println("Enter your employee number: ");
        System.out.println("(Format: NNN-L)");
        number = s.nextLine();
                 
        System.out.println("Enter your hire date: ");
        hireDate = s.nextLine();
         
        System.out.println("Enter your payrate: ");
        payRate = s.nextDouble();
         
        System.out.println("Enter your shift (day=1, night=2): ");
        shift = s.nextInt();

		//make production worker object
        ProductionWorker newWorker = new ProductionWorker(name, number,
        hireDate, shift, payRate);
        
        System.out.println("---- Employee Info ----");
        System.out.println("Name: " + newWorker.getName());
        System.out.println("Hire Date: " + newWorker.getHireDate());
        System.out.println("Pay Rate: " +newWorker.getPayRate());
        System.out.println("Shift: " +newWorker.getShift());
        
        s.close();
	}

}
