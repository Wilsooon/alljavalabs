package ex1;

import java.util.*;
import java.text.DecimalFormat;
import java.text.DateFormat;

public class ProductionWorker extends Employee {
	
	 public static int dayShift =1;
	   public static int nightShift=2;
	   private int shift;
	    
	   private double payRate;
	   private boolean rightShift =false;
	
	   
	   //get shift
	   public String getShift()
	   {
	       String shiftTime = null;
	        
	       if (shift==1)
	            shiftTime = "Day";
	       else if (shift==2) 
	            shiftTime = "Night";
	       
	       return shiftTime;
	    }
	   
	   // set shift
	   public void setShift(int s)
	    {
	        shift = s;
	    }
	     
	   // set pay rate
	   public void setPayRate(double rate)
	   {
	        payRate = rate;    
	   }
	   
	   //get pay rate
	   public double getPayRate()
	   {
	       return payRate;
	    }
	    
	   //constructor to set values for employee info
	   public ProductionWorker(//String name, String number, String hireDate, 
	    String n, String num, String hd, int sh, double rate)
	   {
	       super(n, num, hd);
	       shift = sh;
	       payRate = rate;
	    }
	   
	  //constructor to set vals for production worker info
	   public ProductionWorker(String n, String num, String hd)
	    {
	        super(n, num, hd);
	        shift = dayShift;
	        payRate = 0.0;
	    }
	   
	   //to string
	   public String toString()
	    {
	        DecimalFormat dollar = new DecimalFormat("#,##0.00");
	         
	        String str = super.toString();
	         
	        str+= "\nShift: ";
	        if(shift==dayShift)
	            str+="Day";
	        else if (shift ==nightShift)
	            str+="Nights";
	        else
	            str+= "Invalid Shift Number";
	             
	        str+= ("\n Hourly Pay Rate: $" +
	            dollar.format(payRate));
	             
	        return str;
	    }

}
