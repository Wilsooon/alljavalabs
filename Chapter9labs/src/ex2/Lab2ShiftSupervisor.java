package ex2;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Lab2ShiftSupervisor
{
    public static void main(String[] args)
    {

        int tillGoalIsMet;
        String empNum;
        String empHireDate;
        boolean metGoal = false;
        double annualPay;
        int goal;

        ShiftSupervisor super1 = new ShiftSupervisor();

        super1.setEmpName("Austin Wilson");

        //make scanner
        Scanner input = new Scanner(System.in);

        //decimal format
        DecimalFormat decForm = new DecimalFormat("$#,##0.00");

        //collect and set info
        System.out.println("What is " + super1.getEmpName() + "'s employee ID?");
        empNum = input.nextLine();
        super1.setEmpNum(empNum);

        System.out.println("when was he hired?");
        empHireDate = input.nextLine();
        super1.setEmpHireDate(empHireDate);

        System.out.println("What does he make annually?");
        annualPay = input.nextDouble();
        super1.setAnnual(annualPay);

        System.out.println("How many parts is his goal to produce annually to get his bonus?");
        goal = input.nextInt();

        System.out.println("How many parts did he produce?");
        tillGoalIsMet = input.nextInt();

        //determine if goal is met
        if (tillGoalIsMet >= goal)
        {
            metGoal = true;
        }

        //apply bonus to employee if goal is met
        if (metGoal)
        {
            double bonus = super1.getBonus() + super1.getAnnual();
            System.out.println(super1.getEmpNum() + "Austin met his annual goal.\n" +
                    "He will now receive is bonus of " + decForm.format(super1.getBonus()));
            System.out.println("He now earned " + decForm.format(bonus));
        }
        else
        {
            System.out.println("Austin did not make his annual goal so he doesn't get his bonus" +
                    "\nAND only earns his contracted pay of " + decForm.format(super1.getAnnual()));
        }
        
        input.close();
    }
}