package ex2;

public class ShiftSupervisor extends Employee
{
    private double annualSalary;
    private double bonus = 8052.38;

    
  //default constructor
    public ShiftSupervisor()
    {
    }
    
    //constructor
    public ShiftSupervisor(String name, String number, String hireDate, double annual, double bonus)
    {
        super(name, number, hireDate);
        this.annualSalary = annual;
        this.bonus = bonus;
    }

    //getter / setters
    public double getAnnual()
    {
        return annualSalary;
    }

    public void setAnnual(double annual)
    {
        this.annualSalary = annual;
    }

    public double getBonus()
    {
        return bonus;
    }


    //to string
    @Override
    public String toString()
    {
        return super.toString() + "ShiftSupervisor{" +
                "annual=" + annualSalary +
                ", bonus=" + bonus +
                '}';
    }
}