package ex4;

public class Essay extends GradedActivity
{
    private final double GRAMMAR = 20;
    private final double SPELLING = 20;
    private final double CORRECT_LENGTH = 20;
    private final double CONTENT = 30;

    private double myGrammar;
    private double mySpelling;
    private double myLength;
    private double myContent;
    private double myGrade;

    public Essay(double myGrammar, double mySpelling, double myLength, double myContent, double myGrade)
    {
        this.myGrammar = myGrammar;
        this.mySpelling = mySpelling;
        this.myLength = myLength;
        this.myContent = myContent;
        this.myGrade = myGrade;
    }

    public Essay(){}

    public double getMyGrammar()
    {
        return myGrammar;
    }

    public void setMyGrammar(double myGrammar)
    {
        this.myGrammar = myGrammar;
    }

    public double getMySpelling()
    {
        return mySpelling;
    }

    public void setMySpelling(double mySpelling)
    {
        this.mySpelling = mySpelling;
    }

    public double getMyLength()
    {
        return myLength;
    }

    public void setMyLength(double myLength)
    {
        this.myLength = myLength;
    }

    public double getMyContent()
    {
        return myContent;
    }

    public void setMyContent(double myContent)
    {
        this.myContent = myContent;
    }


    public double myGrade()
    {
        myGrade = myGrammar + mySpelling + myLength + myContent;
        setScore(myGrade);
        return getScore();
    }

}