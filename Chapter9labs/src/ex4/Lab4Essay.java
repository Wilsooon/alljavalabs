package ex4;

import java.util.Scanner;

public class Lab4Essay
{
    public static void main(String[] args)
    {
        double grammar;
        double spelling;
        double length;
        double content;
        // initiate scanner for user input
        Scanner input = new Scanner(System.in);

        Essay paper = new Essay();

        do
        {
            System.out.println("What was your grade on Grammar out of 30 points?");
            grammar = input.nextDouble();
            if (grammar > 30)
            {
                System.out.println("grade has to be below 30 points.");
            }
            else
            {
                paper.setMyGrammar(grammar);
            }
        } while (grammar > 30);


        do
        {
            System.out.println("What was your grade on your Spelling out of 20 points?");
            spelling = input.nextDouble();
            if (spelling > 20)
            {
                System.out.println("grade has to be below 20 points.");
            }
            else
            {
                paper.setMySpelling(spelling);
            }
        } while (spelling > 20);


        do
        {
            System.out.println("What was your grade on your length out of 20 points?");
            length = input.nextDouble();
            if (length > 20)
            {
                System.out.println("grade has to be below 20 points.");

            }
            else
            {
                paper.setMyLength(length);
            }
        } while (length > 20);


        do
        {
            System.out.println("What was your grade on your content out of 30 points?");
            content = input.nextDouble();
            if (content > 30)
            {
                System.out.println("grade has to be below 30 points.");

            }
            else
            {
                paper.setMyContent(content);
            }
        } while (content > 30);

        paper.myGrade();

        System.out.println("Your grade is: " + paper.getGrade());


        input.close();
    }
}